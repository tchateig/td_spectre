#include <emmintrin.h>
#include <x86intrin.h>

#include <stdio.h>
#include <stdint.h>

uint8_t array[256 * 4096];
volatile int temp;
char secret = 94;

// Seuil de temps d'accès considéré comme un succès de cache 
#define CACHE_HIT_THRESHOLD (80)
#define DELTA 1024

// Partie 'FLUSH'
void flushSideChannel()
{
    int i;
    // Ecrire dans le tableau pour forcer l'allocation en RAM (allocation copy-on-write)
    for (i = 0; i < 256; i++)
        array[i * 4096 + DELTA] = 1;

    // Supprimer le tableau du cache
    for (i = 0; i < 256; i++)
        _mm_clflush(&array[i * 4096 + DELTA]);
}

// Partie 'RELOAD'
void reloadSideChannel()
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i;
    for (i = 0; i < 256; i++) {
        addr = &array[i * 4096 + DELTA];
        time1 = __rdtscp(&junk);
        junk = *addr;
        time2 = __rdtscp(&junk) - time1;

        // TODO comment savoir que la donnée est dans le cache ?
        if (/* donnée dans le cache */) {
            printf("array[%d*4096 + %d] est dans le cache.\n", i, DELTA);
            printf("Le secret = %d.\n", i);
        }
    }
}

void victim()
{
    temp = array[secret * 4096 + DELTA];
}

int main(int argc, const char **argv)
{
    // TODO opération FLUSH
    victim();
    // TODO opération RELOAD
    return 0;
}
