#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

// Tableau en mémoire
uint8_t array[10 * 4096];

int main(int argc, const char **argv)
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i;
    // Initialiser la mémoire : force le tableau à être alloué
    for (i = 0; i < 10; i++)
        array[i * 4096] = 1;

    // Vider le tableau du cache
    for (i = 0; i < 10; i++)
    {
        // TODO Vidage d'une ligne de cache
        _mm_clflush(&array[i * 4096]);
    }

    // TODO Accéder à quelques éléments de array

    junk = array[5 * 4096];
    junk = array[8 * 4096];

    // Mesurer les temps d'accès
    for (i = 0; i < 10; i++)
    {
        addr = &array[i * 4096];
        // TODO prendre le timestamp CPU avant lecture
        time1 = __rdtscp(&junk);
        junk = *addr;
        // TODO calculer le temps écoulé dans time2
        time2 = __rdtscp(&junk) - time1;
        printf("Temps d'accès à array[%d * 4096]: %ld cycles CPU.\n", i, time2);
    }
    return 0;
}
